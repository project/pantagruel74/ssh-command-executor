<h1>pantagruel74/ssh-command-executor</h1>

<h2>Requirements</h2>
- PHP >= 7.4
- php-ext-mbstring
- php-ext-json
- openSSH installed on machine
- sshpass installed on machine

<h2>Description</h2>
Package for loading, preparing and executing remote ssh commands.

<h2>Install</h2>
- composer require "pantagruel74/ssh-command-executor"

<h2>Using</h2>
<h3>Services</h3>
<ul>
    <li>SSHConfigLoader - using for loading SSHConfig object from stdObject, array or file</li>
    <li>CommandLoader - using for loading command from file.</li>
    <ul>
        <li>may be configured by SSHConfig, thats will be set to creating Commands</li>
        <li>may be configured by text replace instructions, thats will be set to creating Commands</li>
    </ul>
</ul>

<h3>Objects</h3>
<ul>
    <li>SSHConfig - object used for transport SSHConnection access information, used for configuring SSHCommand</li>
    <li>Command - object for configuring commands list for remote SSH execution and running them</li>
    <ul>
        <li>Should be configured by:</li>
        <ul>
            <li>SSHConfig object</li>
            <li>Text replace instructions (as array type of: [&lt;search&gt; => &lt;replace&gt;, ...])</li>
            <li>String of commands divided by "\n" symbol</li>
        </ul>
        <li>For running command call: Command->executeCommand(): string method.  - result string is the terminal output</li>
    </ul>
</ul>

<h2>License</h2>
- MIT

<h2>Contacts</h2>
- Anatoly Starodubtsev
- Tostar74@mail.ru