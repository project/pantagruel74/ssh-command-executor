<?php

namespace Pantagruel74\SSHCommandExecutor\exceptions;

class SshConfigNotLoadedException extends \RuntimeException
{
    protected $message = 'Ssh config not loaded';
}