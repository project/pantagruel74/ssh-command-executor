<?php

namespace Pantagruel74\SSHCommandExecutor\exceptions;

class AddInstructionIntoMapError extends \RuntimeException
{
    protected $message = "Instructions array should consist of string KEY and string VALUE elements";
}