<?php
declare(strict_types = 1);

namespace Pantagruel74\SSHCommandExecutor\tools;

use Pantagruel74\SSHCommandExecutor\tools\Command;

class CommandLoader
{
    protected TextMap $textReplaceMap;
    protected ?SSHConfig $sshConfig = null;

    /**
     * @param array $textReplaceInstructions
     * @param SSHConfig|null $sshConfig
     */
    public function __construct(array $textReplaceInstructions = [], ?SSHConfig $sshConfig = null)
    {
        $this->textReplaceMap = new TextMap($textReplaceInstructions);
        $this->sshConfig = $sshConfig;
    }

    /**
     * @param SSHConfig|null $sshConfig
     * @return void
     */
    public function setSshConfig(?SSHConfig $sshConfig): void
    {
        $this->sshConfig = $sshConfig;
    }

    public function getSshConfig(): ?SSHConfig
    {
        return $this->sshConfig;
    }

    /**
     * @param string $search
     * @param string $replace
     * @return void
     */
    public function addTextReplaceInstruction(string $search, string $replace): void
    {
        $this->textReplaceMap->addInstruction($search, $replace);
    }

    /**
     * @param array $instructions
     * @return void
     */
    public function addTextReplaceInstructions(array $instructions): void
    {
        $this->textReplaceMap->addInstructions($instructions);
    }

    /**
     * @param string $filePath
     * @return \Pantagruel74\SSHCommandExecutor\tools\Command
     */
    public function loadCommandFromFile(string $filePath): Command
    {
        $command = file_get_contents($filePath);
        $command = str_replace("\r\n", "\n", $command);
        foreach ($this->textReplaceMap->getInstructions() as $textSearch => $textReplace)
        {
            $command = str_replace($textSearch, $textReplace, $command);
        }
        return new Command($command, $this->sshConfig);
    }

}