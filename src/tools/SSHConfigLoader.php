<?php
declare(strict_types = 1);

namespace Pantagruel74\SSHCommandExecutor\tools;

use Pantagruel74\SSHCommandExecutor\exceptions\SshConfigNotLoadedException;
use Pantagruel74\SSHCommandExecutor\tools\SSHConfig;

class SSHConfigLoader
{
    protected string $host = '';
    protected string $user = '';
    protected string $pass = '';

    public function loadConfigFromFile(string $filePath): void
    {
        $ds = DIRECTORY_SEPARATOR;
        $configFile = file_get_contents($filePath);
        $configObj = json_decode($configFile);
        $this->host = $configObj->host;
        $this->user = $configObj->user;
        $this->pass = $configObj->pass;
    }

    public function loadConfigFromObj(object $object): void
    {
        $this->host = $object->host;
        $this->user = $object->user;
        $this->pass = $object->pass;
    }

    public function loadConfigFromArray(array $array): void
    {
        $this->host = $array['host'];
        $this->user = $array['user'];
        $this->pass = $array['pass'];
    }

    public function getConfig(): SSHConfig
    {
        if(empty($this->host) || empty($this->user)) {
            throw new SshConfigNotLoadedException();
        }
        return new SSHConfig(
            $this->host,
            $this->user,
            $this->pass
        );
    }
}