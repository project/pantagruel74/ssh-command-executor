<?php
declare(strict_types = 1);

namespace Pantagruel74\SSHCommandExecutor\tools;

use Pantagruel74\SSHCommandExecutor\exceptions\AddInstructionIntoMapError;

class TextMap
{
    protected array $map = [];

    public function addInstruction(string $index, string $replace): void
    {
        $this->map[$index] = $replace;
    }

    public function addInstructions(array $map): void
    {
        try {
            foreach ($map as $key => $instruction) {
                $this->addInstruction($key, $instruction);
            }
        } catch (\TypeError $typeError) {
            throw new AddInstructionIntoMapError();
        }
    }

    public function __construct(array $map = [])
    {
        $this->addInstructions($map);
    }

    public function getInstructions(): array
    {
        return $this->map;
    }
}