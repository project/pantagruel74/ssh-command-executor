<?php
declare(strict_types = 1);

namespace Pantagruel74\SSHCommandExecutor\tools;

class SSHConfig
{
    protected string $host = "";
    protected string $user = "";
    protected string $password = "";

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     */
    public function __construct(
        string $host,
        string $user,
        string $password
    ) {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }


}