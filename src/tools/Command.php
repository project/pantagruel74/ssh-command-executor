<?php
declare(strict_types = 1);

namespace Pantagruel74\SSHCommandExecutor\tools;

use Pantagruel74\SSHCommandExecutor\exceptions\FileWriteErrorException;
use Ramsey\Uuid\Uuid;

class Command
{
    protected string $command = '';
    protected ?SSHConfig $sshConfig = null;

    /**
     * @param string $command
     * @param SSHConfig|null $sshConfig
     */
    public function __construct(string $command, ?SSHConfig $sshConfig = null)
    {
        $this->command = $command;
        if(isset($sshConfig)) {
            $this->sshConfig = $sshConfig;
        }
    }

    /**
     * @return string
     */
    public function getCommandText(): string
    {
        return $this->command;
    }

    /**
     * @return string
     * @throws FileWriteErrorException
     */
    public function executeCommand(): string
    {
        $runtimeUuid = Uuid::uuid4()->toString();
        $permission = $this->generateScript($runtimeUuid, $this->command);
        $dirPath = $this->generateDirPath($runtimeUuid);
        $ds = DIRECTORY_SEPARATOR;
        shell_exec($dirPath . $ds . "script.sh");
        $fileLog = file_get_contents($dirPath . $ds . "log.log");
        $this->deleteDir($dirPath);
        return $permission . "\n" . $fileLog;
    }

    /**
     * @throws FileWriteErrorException
     */
    protected function generateScript(string $runtimeUuid, string $command): string
    {
        $ds = DIRECTORY_SEPARATOR;
        $dirPath = $this->generateDirPath($runtimeUuid);
        $this->checkBaseDirPath();
        mkdir($dirPath);
        $command = $this->transformCommand($this->command, $dirPath);
        $this->writeScriptSafety($dirPath, $command);
        $result = shell_exec("chmod +x " . $dirPath . $ds . "script.sh");
        if (($result === null) || ($result === false)) {
            return '';
        }
        return $result;
    }

    /**
     * @param string $command
     * @param string $dirPath
     * @return string
     */
    protected function transformCommand(string $command, string $dirPath): string
    {
        $ds = DIRECTORY_SEPARATOR;
        if(isset($this->sshConfig)) {
            $command = $this->sshTransformCommand($command, $this->sshConfig);
        }
        $command = $this->outStreamsTransformCommands($command, $dirPath . $ds . "log.log");
        return $command;
    }

    /**
     * @param string $dirPath
     * @param string $command
     * @return void
     * @throws FileWriteErrorException
     */
    protected function writeScriptSafety(string $dirPath, string $command): void
    {
        $ds = DIRECTORY_SEPARATOR;
        if(file_put_contents($dirPath . $ds . "script.sh", "#!/bin/bash\n" . $command) == 0) {
            $this->deleteDir($dirPath);
            throw new FileWriteErrorException("Can't write file " . $dirPath . $ds . "script.sh : 0 bytes was wrote");
        }
    }

    /**
     * @param string $runtimeUuid
     * @return string
     */
    protected function generateDirPath(string $runtimeUuid): string
    {
        $ds = DIRECTORY_SEPARATOR;
        return __DIR__ . $ds . ".." . $ds . ".." . $ds . "tmp" . $ds . $runtimeUuid;
    }

    /**
     * @return void
     */
    protected function checkBaseDirPath()
    {
        $ds = DIRECTORY_SEPARATOR;
        if(!is_dir(__DIR__ . $ds . ".." . $ds . ".." . $ds . "tmp")) {
            mkdir(__DIR__ . $ds . ".." . $ds . ".." . $ds . "tmp");
        }
    }

    /**
     * @param string $commandText
     * @param $logFullFileName
     * @return string
     */
    protected function outStreamsTransformCommands(string $commandText, $logFullFileName): string
    {
        $logFullFileName = str_replace("\\", "/", $logFullFileName);
        $commandLines = explode("\n", $commandText);
        foreach ($commandLines as $commandLineIndex => &$commandLineVal)
        {
            $commandLineVal .= ($commandLineIndex == 0 ? " 1>" : " 1>>") . $logFullFileName
                . " 2>>" . $logFullFileName;
        }
        return implode("\n", $commandLines);
    }

    /**
     * @param string $commandText
     * @param SSHConfig $sshConfig
     * @return string
     */
    protected function sshTransformCommand(string $commandText, SSHConfig $sshConfig): string
    {
        $sshPassword = $this->preparePassword($sshConfig->getPassword());
        $sshPrefix = "sshpass -p " . $sshPassword . " ssh -o StrictHostKeyChecking=no " . $sshConfig->getUser()
            . "@" . $sshConfig->getHost();
        $commandLines = explode("\n", $commandText);
        foreach ($commandLines as &$commandLineVal)
        {
            $commandLineVal = $sshPrefix . " " . $commandLineVal;
        }
        return implode("\n", $commandLines);
    }

    /**
     * @param string $password
     * @return string
     */
    protected function preparePassword(string $password): string
    {
        if(mb_substr($password, 0, 1) === "!") {
            return "\\" . $password;
        }
        return $password;
    }

    /**
     * @param string $dir
     * @return void
     */
    protected function deleteDir(string $dir): void
    {
        $d = opendir($dir);
        while(($entry = readdir($d)) !== false)
        {
            if ($entry != "." && $entry != "..")
            {
                if (is_dir($dir."/".$entry))
                {
                    $this->deleteDir($dir."/".$entry);
                } else {
                    unlink ($dir."/".$entry);
                }
            }
        }
        closedir($d);
        rmdir ($dir);
    }

    /**
     * @return SSHConfig|null
     */
    public function getSshConfig(): ?SSHConfig
    {
        return $this->sshConfig;
    }

    /**
     * @param SSHConfig|null $sshConfig
     */
    public function setSshConfig(?SSHConfig $sshConfig): void
    {
        $this->sshConfig = $sshConfig;
    }

    public function replaceText(string $search, string $replace): void
    {
        $this->command = str_replace($search, $replace, $this->command);
    }

    public function replaceManyPhrases(array $replaceArr): void
    {
        $textMap = new TextMap($replaceArr);
        foreach ($textMap->getInstructions() as $search => $replace)
        {
            $this->replaceText($search, $replace);
        }
    }



}