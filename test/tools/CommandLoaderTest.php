<?php

use Pantagruel74\SSHCommandExecutor\tools\CommandLoader;
use Pantagruel74\SSHCommandExecutor\tools\Command;
use Pantagruel74\SSHCommandExecutor\tools\SSHConfig;

class CommandLoaderTest extends \PHPUnit\Framework\TestCase
{
    public function testConstruct()
    {
        $ds = DIRECTORY_SEPARATOR;
        $commandLoader = new CommandLoader();
        $command = $commandLoader->loadCommandFromFile(
            __DIR__ . $ds . '..' . $ds . 'test-config' . $ds . 'commandPure.txt');
        $this->assertEquals($command->getCommandText(), "pwd\necho 111\nls");
        $this->assertEquals($command->getSshConfig(), null);
    }

    public function testConstructWithSshConfig()
    {
        $ds = DIRECTORY_SEPARATOR;
        $sshComfig = new SSHConfig('someHost', "someUser", "somePass");
        $commandLoader = new CommandLoader([], $sshComfig);
        $command = $commandLoader->loadCommandFromFile(
            __DIR__ . $ds . '..' . $ds . 'test-config' . $ds . 'commandPure.txt');
        $this->assertEquals($command->getCommandText(), "pwd\necho 111\nls");
        $this->assertEquals($command->getSshConfig(), $sshComfig);
    }

    public function testReplaceInstructions()
    {
        $ds = DIRECTORY_SEPARATOR;
        $commandLoader = new CommandLoader([
            "{{dirname}}" => "/usr/home",
            "{{echo}}" => "success!"
        ]);
        $command = $commandLoader->loadCommandFromFile(
            __DIR__ . $ds . '..' . $ds . 'test-config' . $ds . 'commandWithReplaces.txt');
        $this->assertEquals($command->getCommandText(), "mkdir /usr/home\ncd /usr/home\necho success!");

        $commandLoader->addTextReplaceInstruction("{{dirname}}", "/var/www");
        $command = $commandLoader->loadCommandFromFile(
            __DIR__ . $ds . '..' . $ds . 'test-config' . $ds . 'commandWithReplaces.txt');
        $this->assertEquals($command->getCommandText(), "mkdir /var/www\ncd /var/www\necho success!");
    }


}