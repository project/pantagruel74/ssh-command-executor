<?php

use Pantagruel74\SSHCommandExecutor\tools\Command;
use Pantagruel74\SSHCommandExecutor\tools\SSHConfig;

class CommandTest extends \PHPUnit\Framework\TestCase
{
    public function testContructor()
    {
        $command = new Command('dac98c1');
        $this->assertEquals($command->getCommandText(), 'dac98c1');
    }

    public function testExecuteCommand()
    {
        $command = new Command("echo !!!\necho 222");
        $this->assertEquals("\n!!!\n222\n", $command->executeCommand());
        $filesInFolder = [];
        $ds = DIRECTORY_SEPARATOR;
        foreach (new DirectoryIterator(__DIR__ . $ds . ".." . $ds . ".." . $ds . "tmp") as $fileInfo) {
            if ($fileInfo->isDot()) continue;
            $filesInFolder[] = $fileInfo->getFilename();
        }
        $this->assertEmpty($filesInFolder);
    }

    public function testTransformCommand()
    {
        $command = new class("echo !!!\necho 222", new SSHConfig('localhost', 'user', 'pass')) extends Command
        {
            public function transformCommand(string $command, string $dirPath): string
            {
                return parent::transformCommand($command, $dirPath);
            }
        };
        $this->assertEquals(
            "sshpass -p pass ssh -o StrictHostKeyChecking=no user@localhost echo !!! 1>/dirdir/log.log 2>>/dirdir/log.log\n"
            . "sshpass -p pass ssh -o StrictHostKeyChecking=no user@localhost echo 222 1>>/dirdir/log.log 2>>/dirdir/log.log",
            $command->transformCommand($command->getCommandText(), '/dirdir')
        );
    }

    public function testAddOutputIntoCommands()
    {
        $command = new class('') extends Command {
            public function outStreamsTransformCommands($commandText, $logFullFileName): string
            {
                return parent::outStreamsTransformCommands($commandText, $logFullFileName);
            }
        };
        $this->assertEquals(
            $command->outStreamsTransformCommands("echo !!!\necho 222", "C:\wamp\log.log"),
            "echo !!! 1>C:/wamp/log.log 2>>C:/wamp/log.log\necho 222 1>>C:/wamp/log.log 2>>C:/wamp/log.log"
        );
    }

    public function testSshConfigGetterAndSetter()
    {
        $command = new Command('');
        $this->assertEquals($command->getSshConfig(), null);

        $sshComfig = new SSHConfig('someHost', "someUser", "somePass");
        $command->setSshConfig($sshComfig);
        $this->assertEquals($command->getSshConfig(), $sshComfig);
    }

}