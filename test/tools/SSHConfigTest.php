<?php
use Pantagruel74\SSHCommandExecutor\tools\SSHConfig;

class SSHConfigTest extends \PHPUnit\Framework\TestCase
{
    public function testSshConfig()
    {
        $config = new SSHConfig('host123', 'user1576', 'pass17bf');
        $this->assertEquals($config->getHost(), 'host123');
        $this->assertEquals($config->getUser(), 'user1576');
        $this->assertEquals($config->getPassword(), 'pass17bf');
    }
}