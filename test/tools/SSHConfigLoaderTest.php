<?php

use Pantagruel74\SSHCommandExecutor\tools\SSHConfigLoader;

class SSHConfigLoaderTest extends \PHPUnit\Framework\TestCase
{
    public function testLoadConfig()
    {
        $ds = DIRECTORY_SEPARATOR;
        $configPath = __DIR__ . $ds . ".." . $ds . "test-config" . $ds . "ssh.json";
        $sshConfigLoader = new SSHConfigLoader();
        $sshConfigLoader->loadConfigFromFile($configPath);
        $sshConfig = $sshConfigLoader->getConfig();

        $this->assertEquals($sshConfig->getHost(), "37.140.192.116");
        $this->assertEquals($sshConfig->getUser(), "as7f9s");
        $this->assertEquals($sshConfig->getPassword(), "!asfj8asf90");
    }

    public function testGetConfig()
    {
        $ds = DIRECTORY_SEPARATOR;

        $sshConfigLoader = new SSHConfigLoader();
        $sshConfigLoader->loadConfigFromObj((object) [
            "host" => "37.140.192.116",
            "user" => "as7f9s",
            "pass" => "!asfj8asf90",
        ]);
        $sshConfig = $sshConfigLoader->getConfig();

        $this->assertEquals($sshConfig->getHost(), "37.140.192.116");
        $this->assertEquals($sshConfig->getUser(), "as7f9s");
        $this->assertEquals($sshConfig->getPassword(), "!asfj8asf90");
    }
}