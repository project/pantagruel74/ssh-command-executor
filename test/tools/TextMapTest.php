<?php

use Pantagruel74\SSHCommandExecutor\tools\TextMap;

class TextMapTest extends \PHPUnit\Framework\TestCase
{
    public function testConstructor()
    {
        $instructions = [
            "alpha" => "beta",
            "gamma" => "delta",
        ];
        $textMap = new TextMap($instructions);
        $this->assertEquals($textMap->getInstructions(), $instructions);

        $instructions = [
            "alpha" => "beta",
            "gamma" => ["delta", "omega"],
        ];
        $this->expectException(\Pantagruel74\SSHCommandExecutor\exceptions\AddInstructionIntoMapError::class);
        $textMap = new TextMap($instructions);
    }
}